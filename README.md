# Rewrite manga with comments

Process:

1. Open a chapter
2. Check the `files-list` entries
3. Save the url of the entries with number of comments > 0
4. For each url open a new tab
5. Switch to the tab, 
6. Get the comments
7. Return to the original tab
8. Close the tab
9. When all the url have been visited search for the next chapter url
10. Repeat from 1


