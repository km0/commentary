from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from . import avatar
import ssl
import os
import json


def get_next(driver):
    try:
        next = driver.find_element(By.PARTIAL_LINK_TEXT, "Next Chapter")
        url = next.get_attribute('href')
        # if input('Continue with next chapter? (y/n) ') == 'y':
        driver.get(url)
        # driver.implicitly_wait(2)
        get_files(driver)
    except NoSuchElementException:
        pass
    return


def get_files(driver):
    original_window = driver.current_window_handle
    containers = driver.find_elements(
                By.XPATH,
                "//i[@name='comments']/parent::div/parent::div/parent::div"
                )
    for container in containers:
        comments = container.find_element(
                By.CSS_SELECTOR,
                "i[name=comments] + span"
                ).text
        if (comments == '0'):
            continue
        url = container.find_element(By.CSS_SELECTOR, "a").get_attribute('href')
        driver.switch_to.new_window('tab')
        driver = get_chapter(driver, url)
        driver.close()
        driver.switch_to.window(original_window)
    get_next(driver)


def get_chapter(driver, url):
    driver.get(url)
    driver.implicitly_wait(2)
    print(driver.title)

    try:
        comments_section = driver.find_element(By.CSS_SELECTOR, '[data-name=review-section]')
        load_more = comments_section.find_element(By.XPATH, '//button[text()="Load More"]')
        load_more.click()
    except NoSuchElementException:
        pass


    show_replies_buttons = driver.find_elements(By.XPATH, "//button[contains(., 'repl')]")

    while len(show_replies_buttons) > 0:
        button = show_replies_buttons.pop()
        try:
            button.click()
        except ElementClickInterceptedException:
            pass
        show_replies_buttons = driver.find_elements(By.XPATH, "//button[contains(., 'replies')]")

    comments_section = driver.find_element(By.CSS_SELECTOR, '[data-name=review-section]')
    messages = comments_section.find_elements(By.CSS_SELECTOR, '[data-name=comment-item]')
    print(f'{len(messages)} total comments')

    comments = []

    for message in messages:
        username = message.find_element(By.CSS_SELECTOR, '.ml-2.flex.flex-col div').text
        try:
            avatar_url = message.find_element(By.CSS_SELECTOR, '.avatar img').get_attribute('src')
            img = avatar.get_avatar(avatar_url, username)
        except NoSuchElementException:
            img = ""
        comment = message.find_element(By.CSS_SELECTOR, '.my-2 react-island').text
        comments.append({
            "username": username,
            "avatar": img,
            "comment": comment
            })

    if len(comments) > 0:
        rest, _, slug = url.rpartition('/')
        _, _, series = rest.rpartition('/')
        os.makedirs(f'output/{series}', exist_ok=True)

        with open(f"output/{series}/{slug}.json", 'w') as f:
            f.write(json.dumps({
                "title": driver.title,
                "slug": slug,
                "url": url,
                "comments": comments
            }, indent=2))

    return driver


def start():
    # Set unverified context to prevent [SSL: CERTIFICATE_VERIFY_FAILED] error
    ssl._create_default_https_context = ssl._create_unverified_context

    url = 'https://mangapark.net/title/125883-en-survival-story-of-a-sword-king-in-a-fantasy-world/2334982-ch-1'
    url = 'https://mangapark.net/title/75577-en-solo-leveling/1703520-ch-0' 
    driver = webdriver.Chrome()
    driver.get(url)
    # driver.implicitly_wait(2)
    get_files(driver)




if __name__ == "__main__":
    get_chapter()
