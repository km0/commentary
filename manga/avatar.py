from glob import glob
import json
import requests
import shutil
import os
import mimetypes

def get_avatar(url, username):
    try:
        res = requests.get(url, stream=True)
        if res.status_code == 200:
            format = mimetypes.guess_extension(res.headers['Content-Type'])
            file_name = username + format
            output = os.path.join('output', 'avatar', file_name)
            with open(output, 'wb') as f:
                shutil.copyfileobj(res.raw, f)
            print('Image sucessfully Downloaded: ', file_name)
            return output
    except:
        print('Image Couldn\'t be retrieved')
        return ""


def all():
    os.makedirs('output/avatar', exist_ok=True)
    for file in glob('output/75577-en-solo-leveling/*.json'):
        with open(file, 'r') as f:
            chapter = json.load(f)
            for comment in chapter['comments']:
                if comment['avatar'] != "":
                    get_avatar(comment['avatar'], comment['username'])
